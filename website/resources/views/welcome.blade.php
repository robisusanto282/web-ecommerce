@extends('layouts.main')
    <!-- ===========Hero Section===================== -->
@section('content')
    <main class="hero-section">
        <div>
          <h1 class="fs-200 fs-poppins">
            Beats Solo
            <span class="block lineheight fs-300 bold-900 big-wireless fs-poppins"
              >Wireless</span
            ><span
              class="text-white fs-900 uppercase lineheight-2 bold-bolder fs-poppins"
              >HeadPhone</span
            >
          </h1>
          <img src="image/image1.png" alt="" />
        </div>
        <div class="hero-inner flex">
          <div>
            <a href="/product1"><button class="large-btn bg-red text-white fs-poppins fs-50">
              Pesan Sekarang!
            </button>
            </a>
          </div>
          <div class="hero-info">
            <h4 class="fs-montserrat">Description</h4>
            <p class="fs-montserrat">
              Headset bluetooth minimalis untuk anak muda jaman now
            </p>
          </div>
        </div>
      </main>
  
      <!-- =================Product Section======================= -->
  
      <section class="product-section">
        <div class="category bg-black grid">
          <div>
            <h3 class="text-white fs-50 fs-montserrat bold">
              Enjoy <span class="block fs-300 fs-poppins bold">With</span
              ><span
                class="earphone uppercase fs-400 fs-montserrat bold-900 lineheight"
                >Earphone</span
              >
            </h3>
            <button class="prdduct-btn large-btn text-white bg-red fs-montserrat">
              Browse
            </button>
          </div>
          <div class="product-img1">
            <img src="image/h.png" alt="" />
          </div>
        </div>
        <div class="category bg-yellow grid">
          <div>
            <h3 class="text-white fs-50 fs-montserrat bold">
              New <span class="block fs-300 fs-poppins bold">Wear</span
              ><span
                class="earphone uppercase fs-400 fs-montserrat bold-900 lineheight"
                >Gadget</span
              >
            </h3>
            <button
              class="prdduct-btn large-btn text-yellow bg-white fs-montserrat"
            >
              Browse
            </button>
          </div>
          <div class="product-img2">
            <img src="image/w.png" alt="" />
          </div>
        </div>
        <div class="category bg-red grid">
          <div>
            <h3 class="text-white fs-50 fs-montserrat bold">
              Trend <span class="block fs-300 fs-poppins bold">Devices</span
              ><span
                class="earphone uppercase fs-400 fs-poppins bold-900 lineheight"
                >Laptop</span
              >
            </h3>
            <button class="prdduct-btn large-btn text-red bg-white fs-poppins">
              Browse
            </button>
          </div>
          <div class="product-img3">
            <img src="image/Laptop.png" alt="" />
          </div>
        </div>
        <div class="category bg-gray grid">
          <div>
            <h3 class="text-black fs-50 fs-poppins bold">
              Best
              <span class="block fs-300 fs-poppins bold text-black">Gaming</span
              ><span
                class="earphone uppercase fs-400 fs-poppins bold-900 lineheight"
                >Console</span
              >
            </h3>
            <button class="prdduct-btn large-btn text-white bg-red fs-poppins">
              Browse
            </button>
          </div>
          <div class="product-img4">
            <img src="image/gam.png" alt="" />
          </div>
        </div>
        <div class="category bg-green grid">
          <div>
            <h3 class="text-white fs-50 fs-poppins bold">
              Play <span class="block fs-300 fs-poppins bold">Game</span
              ><span
                class="earphone uppercase fs-400 fs-poppins bold-900 lineheight"
                >Oculus</span
              >
            </h3>
            <button class="prdduct-btn large-btn text-green bg-white fs-poppins">
              Browse
            </button>
          </div>
          <div class="product-img5">
            <img src="image/man2.png" alt="" />
          </div>
        </div>
        <div class="category bg-blue grid">
          <div>
            <h3 class="text-white fs-50 fs-poppins bold">
              New <span class="block fs-300 fs-poppins bold">Amazon</span
              ><span
                class="earphone uppercase fs-400 fs-poppins bold-900 lineheight"
                >speaker</span
              >
            </h3>
            <button class="prdduct-btn large-btn text-blue bg-white fs-poppins">
              Browse
            </button>
          </div>
          <div class="product-img6">
            <img src="image/mus.png" alt="" />
          </div>
        </div>
      </section>
  
      <!-- ========================================= -->
      <!-- =============Service section============== -->
  
      <section class="service-section">
        <div class="service">
          <img src="image/free.svg" alt="" />
          <div class="service-info">
            <h3 class="fs-poppins fs-200">Free Shippng</h3>
            <p class="fs-montserrat fs-50">Free Shipping On All Order</p>
          </div>
        </div>
        <div class="service">
          <img src="image/sett.svg" alt="" />
          <div class="service-info">
            <h3 class="fs-poppins fs-200">Money Guarantee</h3>
            <p class="fs-montserrat fs-50">30 Day Money Back</p>
          </div>
        </div>
        <div class="service">
          <img src="image/supt.svg" alt="" />
          <div class="service-info">
            <h3 class="fs-poppins fs-200">Online Support 24/7</h3>
            <p class="fs-montserrat fs-50">Technical Support 24/7</p>
          </div>
        </div>
        <div class="service">
          <img src="image/pay.svg" alt="" />
          <div class="service-info">
            <h3 class="fs-poppins fs-200">Secure Payment</h3>
            <p class="fs-montserrat fs-50">All Cards Accepted</p>
          </div>
        </div>
      </section>
  
      <!-- ===================Feature Section============= -->
  
      <section class="feature-section bg-red">
        <div class="feature-one grid">
          <img src="image/p-1.png" alt="" />
          <p class="text-white uppercase">20% OFF</p>
          <p
            class="font-size lineheight fs-500 text-white fs-poppins bold-900 uppercase"
          >
            fine <span class="smile">smile</span>
          </p>
        </div>
        <div class="feature-info">
          <h2 class="fs-200 text-white fs-poppins bold-500">Beats Solo Air</h2>
          <p class="fs-poppins fs-300 bold-800 text-white">Promo 5/5</p>
          <button class="prdduct-btn large-btn text-red bg-white fs-poppins">
            Shop
          </button>
        </div>
      </section>
      <!-- =============================Best Sellar================== -->
  
      <section class="best-product container">
        <h2 class="letter-spacing bold-800 fs-poppins">Best Seller Products</h2>
        <p class="fs-montserrat fs-100">
          The most valuable product to buy 
        </p>
      </section>
  
      <!-- ===========================Heading======================== -->

      <section class="best-Seller">
      @foreach($data as $datas)
        <div class="product grid">
          <img src="{{$datas->gambar_produk}}" alt="" />
          <p class="fs-poppins bold-500">{{$datas->nama_produk}}</p>
          <p class="fs-poppins bold-500">{{$datas->title}}</p>
            <p class="fs-poppins bold-500">Rp.{{$datas->harga}}</p>
          <div class="product-details grid bg-red">
            <i class="text-white uil uil-shopping-cart-alt"></i>
            <i class="text-white uil uil-heart-alt"></i>
          </div>
        </div>
      @endforeach
      <!-- batas -->
      </section>
      @include('partial.recent-news')
  @endsection
  
      <!-- =========================================== -->
