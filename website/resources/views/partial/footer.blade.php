<footer>


    <!-- =============Footer Menu=================== -->
    <section class="footer grid">
      <div class="footer-logo grid">
        <img src="image/logo.png" width="200" alt="" />
        <p class="fs-montserrat fs-200">
          Let's join us.
        <div class="social-media flex">
          <i class="uil uil-facebook-f"></i>
          <i class="uil uil-instagram"></i>
          <i class="uil uil-linkedin"></i>
          <i class="uil uil-twitter"></i>
        </div>
      </div>

      <div class="footer-menu grid">
        <h3 class="fs-poppins fs-200 bold-800">Quick Links</h3>
        <ul>
          <li>
            <a class="fs-montserrat text-black fs-200" href="/">Home</a>
          </li>
          <li>
            <a class="fs-montserrat text-black fs-200" href="/about">About</a>
          </li>
          <li>
            <a class="fs-montserrat text-black fs-200" href="#">Shop</a>
          </li>
          <li>
            <a class="fs-montserrat text-black fs-200" href="#">Contact</a>
          </li>
        </ul>
      </div>

      <div class="contact grid">
        <h3 class="fs-poppins fs-200 bold-800">Contact</h3>
        <p class="fs-montserrat">
          +(62)896-8888-9999 Ferry Fransisco <br>
          +(62)897-1234-5678 Robby Susanto
        </p>
      </div>

      <div class="emails grid">
        <h3 class="fs-poppins fs-200 bold-800">Subscribe To Our Email</h3>
        <p class="updates fs-poppins fs-300 bold-800">
          For Latest News & Updates
        </p>
        <div class="inputField flex bg-gray">
          <input
            type="email"
            placeholder="Enter Your Email"
            class="fs-montserrat bg-gray"
          />
          <button class="bg-red text-white fs-poppins fs-50">
            Subscribe
          </button>
        </div>
      </div>
    </section>

    <section class="copyRight">
      <p class="c-font fs-montserrat fs-200">
        © 2022 eStore. All rights reserved.
      </p>
      <p class="fs-montserrat fs-100 text-align p-top">
        Privacy Policy . Term Condition
      </p>
    </section>
  </footer>