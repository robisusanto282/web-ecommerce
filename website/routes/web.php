<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomAuthController;
use App\Http\Controllers\Produkcontroller;

Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/', function () {
    return redirect('login');
});

Route::controller(CustomAuthController::class)->group(function () {
    Route::get('/login', 'index')->name('login');
    Route::post('/custom-login', 'customLogin')->name('login.custom');
    Route::get('/register', 'register')->name('register-user');
    Route::post('/custom-registration', 'customRegister')->name('register.custom');
    Route::get('signout','signOut')->name('signout');
});

Route::controller(Produkcontroller::class)->group(function () {
    Route::get('/homepage', 'produk')->name('homepage');
});



Route::get('/about', function(){
    return view('about', [
        "title" => "About",
    ]);
});
Route::get('/product1', function(){
    return view('product1',[
        "title" => "Headset 1",
    ]);
});